set term png
set output "plots/shove-insert-push.png"
set key top left
set yrange [0:2e-4]
set pointsize .5
set xtics rotate by -90
plot "./data/chain_shove", "./data/chain_insert", "./data/chain_push"

set term dumb
set output "plots/shove-insert-push.ascii"
set key top left
set yrange [0:2e-4]
set pointsize .5
set xtics rotate by -90
plot "./data/chain_shove", "./data/chain_insert", "./data/chain_push"
