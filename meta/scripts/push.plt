set term png
set output "plots/push.png"
set key top left
set yrange [0:2e-4]
set pointsize .5
set xtics rotate by -90
plot "./data/chain_push"

set term dumb
set output "plots/push.ascii"
set key top left
set yrange [0:2e-4]
plot "./data/chain_push"
