/*

    T O D O: real testing XXX

*/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "chain.h"
#include "benchmark.h"

#define TEST(X) if (!(X)) {fprintf(stderr, "failed test at line %d!\n", __LINE__); \
        for (int i = 0; i < chain_len(buf); ++i) printf(" %d", buf[i]); puts("");return 1;}
int error_count=0;        
void error_func(){
    error_count++;
}
int main (int argc, char **argv){
    int* buf = chain_new(int);
    
    chain_push(buf, 1);
    chain_push(buf, 2);
    chain_push(buf, 3);
    chain_push(buf, 4);
    TEST(!memcmp(buf, (int[]){1, 2, 3, 4}, 4*sizeof(int)));
    
    chain_insert(buf, 1, -1);
    chain_insert(buf, 3, -2);
    TEST(!memcmp(buf, (int[]){1, -1, 2, -2, 3, 4}, 6*sizeof(int)));
    
    chain_remove(buf, 1);
    TEST(!memcmp(buf, (int[]){1, 2, -2, 3, 4}, 5*sizeof(int)));
    
    chain_shove(buf, 100);
    TEST(!memcmp(buf, (int[]){100, 1, 2, -2, 3, 4}, 6*sizeof(int)));
    
    chain_clip(buf, 3);
    TEST(!memcmp(buf, (int[]){100, 1, 2}, 3*sizeof(int)));
    
    TEST(chain_pop(buf)==2);
    TEST(!memcmp(buf, (int[]){100, 1}, 2*sizeof(int)));
    TEST(chain_len(buf) == 2);
    TEST(buf[chain_len(buf)] == 2) // temp area ; not included with the array
    
    chain_push_all(buf, ((int[]){200, 2}), 2);
    TEST(!memcmp(buf, (int[]){100, 1, 200, 2}, 4*sizeof(int)));
    
    chain_insert_all(buf, 2, ((int[]){4000, 4}), 2);
    TEST(!memcmp(buf, (int[]){100, 1, 4000, 4, 200, 2}, 6*sizeof(int)));
        
    chain_insert(buf, chain_len(buf)-1, 0);
    TEST(!memcmp(buf, (int[]){100, 1, 4000, 4, 200, 0, 2}, 7*sizeof(int)));
    
    
    
    /*limits*/
    chain_error=error_func;
    chain_clip(buf, 0);
    TEST(!chain_len(buf));
    chain_free(buf);
    buf=chain_new(int);
    chain_shove(buf, 16657);
    chain_pop(buf);
    TEST(buf[0]==16657);
    chain_free(buf);
    
    buf = chain_newl(int, 40);
    for (int i = 0; i < 40; ++i) buf[i] = 12;
    chain_free(buf);
    
    buf = chain_newa(int, 6, ((int []){0, 1, 4, 9, 16, 25}));
    chain_push(buf, 36);
    for (int i = 0; i < 7; ++i) TEST(buf[i] == i*i);
    chain_free(buf);
    
    
    if (argc>1 && !strcmp("--benchmark", argv[1])) {
        int *buf;
        unsigned int i, am = 600000;
        FILE *fd;
        
        fprintf(stderr, "generating data/chain_shove... ");
        cl_init((int)am);
        buf = chain_new(int);
        fd = fopen("data/chain_shove", "w");
        cl_set();
        for (i=0;i<am;++i) {
            chain_shove(buf, i);
            cl_mark();
        }
        cl_print(fd);
        fclose(fd);
        chain_free(buf);
        
        fprintf(stderr, "done!\ngenerating data/chain_insert (at midlengths)... ");
        
        cl_reset();
        buf = chain_new(int);
        fd = fopen("data/chain_insert", "w");
        cl_set();
        for (i=0;i<am;++i) {
            chain_insert(buf ,i/2, i);
            cl_mark();
        }
        cl_print(fd);
        fclose(fd);
        chain_free(buf);
        
        fprintf(stderr, "done!\ngenerating data/chain_push... ");
        
        cl_reset();
        buf = chain_new(int);
        fd = fopen("data/chain_push", "w");
        cl_set();
        for (i=0;i<am;++i) {
            chain_push(buf, i);
            cl_mark();
        }
        cl_print(fd);
        fclose(fd);
        chain_free(buf);
        fprintf(stderr, "done!\n");
    }
    
    
    puts("done");
    return 0;
}
