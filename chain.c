/*    
    Chains: Native dynamic arrays for any type of variable in a convenient 
              package (.c/.h)

    Copyright (C) 2012  Sulaiman A. Mustafa
    
    [ Alternative licensing is available ]
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    
    Contact me if an alternative license is desired. Depending on the type of 
    intended use, an alternative license might be granted free of charge, in 
    exchange for compensation, or it might not be granted at all.
    
    Contact: http://people.sigh.asia/~sulaiman/about/contact/
    
    -----------------------------------------------------------------------
    
    TODO:
    
        chain_sub(array, from, to) // use chain_newa_
        except -1 (python like)
        
        
        update documentation XXX
        



*/

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "chain.h"

/*
MEMORY STRUCTURE

    {size, length, [outside world ptr], ..., temp cell}
    an empty chain array is sizeof(uint32)*2+size_of_cell long
*/

void chain_error_(){
    abort();
}

void* chain_new_(short size){
    void *c;
    if (!size) {chain_error(); return NULL;} 
    if (!(c = malloc(size+CHAIN_MTS))) {chain_error(); return NULL;} 
    *((int *) c) = size;
    *(((int *) c)+1) = 0;
    return c+CHAIN_MTS;
}

void* chain_newl_(short size, int length){
    void *c;
    if (!size) {chain_error(); return NULL;} 
    if (!(c = malloc(size+CHAIN_MTS+size*length))) {chain_error(); return NULL;} 
    *((int *) c) = size;
    *(((int *) c)+1) = length;
    return c+CHAIN_MTS;
}

void* chain_newa_(short size, int length, void* src){
    void *c;
    if (!size) {chain_error(); return NULL;} 
    if (!(c = malloc(size+CHAIN_MTS+size*length))) {chain_error(); return NULL;} 
    *((int *) c) = size;
    *(((int *) c)+1) = length;
    memcpy(c+CHAIN_MTS, src, size*length);
    return c+CHAIN_MTS;
}

void chain_free(void *c){ free(c-CHAIN_MTS);}


void* chain_resize_(void *c, int where, short how){
    int size, length;
    c-=CHAIN_MTS;
    size = *((int *) c);
    length = *(((int *) c)+1);
    
    if (how == 1) {
        if (length == INT_MAX) {chain_error(); return NULL;} 
        if (!(c=realloc(c, (CHAIN_MTS+size)+size*length+size))) {chain_error(); return NULL;} 
        if (where< length) {
            memmove(c+CHAIN_MTS+where*size+size, c+CHAIN_MTS+where*size, (CHAIN_MTS+length*size)-(CHAIN_MTS+where*size));
        }
        ++(*(((int *) c)+1));
    }
    else if (how == -1) {
        if (!length) {chain_error(); return NULL;} 
        if (where< length-1) {
            memmove(c+CHAIN_MTS+length*size, c+CHAIN_MTS+where*size, size);
            memmove(c+CHAIN_MTS+where*size, c+CHAIN_MTS+where*size+size, (CHAIN_MTS+length*size)-(CHAIN_MTS+where*size+size)+size);
        }
        else{
            memmove(c+CHAIN_MTS+length*size, c+CHAIN_MTS+length*size-size, size);
        }
        if (!(c=realloc(c, (CHAIN_MTS+size)+size*length-size))) {chain_error(); return NULL;} 
        (*(((int *) c)+1))-=1;
        
    }
    else {chain_error(); return NULL;} 
    return c+CHAIN_MTS;
}



void* chain_insertm_(void *c, int where, void *in, int len){
    int size, length;
    c-=CHAIN_MTS;
    size = *((int *) c);
    length = *(((int *) c)+1);
    
    if (len+length > INT_MAX) {chain_error(); return NULL;} 
    if (where>length) where=length;
    
    if (!(c=realloc(c, (CHAIN_MTS+size)+size*(length+len)))) {chain_error(); return NULL;} 
    memmove(c+CHAIN_MTS+(where+len)*size, c+CHAIN_MTS+(where*size), size*(length-where));
    memmove(c+CHAIN_MTS+(where*size), in, size*len);
    
    *(((int *) c)+1)+=len;
    return c+CHAIN_MTS;
}

void* chain_clip_(void *c, int where){
    int size, length;
    c-=CHAIN_MTS;
    size = *((int *) c);
    length = *(((int *) c)+1);    
    
    if(where>=length) return c+CHAIN_MTS;
    
    if (!(c=realloc(c, (CHAIN_MTS+size)+size*where))) {chain_error(); return NULL;} 
    *(((int *) c)+1)=where;
    
    return c+CHAIN_MTS;
}


void (*chain_error)()=chain_error_;
